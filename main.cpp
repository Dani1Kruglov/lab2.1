
#include <iostream>
#include <vector>
#include <chrono>
// Длина последовательности: 10
// Последовательность: 1 2 100 35 58 39 44 30 27 59

using namespace std;


class Timer
{
private:

    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<double, std::ratio<1> >;

    std::chrono::time_point<clock_t> m_beg;

public:
    Timer() : m_beg(clock_t::now())
    {
    }

    void reset()
    {
        m_beg = clock_t::now();
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};

int main()
{

    int a;
    cout << "Введите количество чисел в последовательности: ";
    cin >> a;
    vector<int> v(a);
    cout << "Введите саму последовательность: ";

    for (int i = 0; i < a; i++)// ввод последовательности
        cin >> v[i];

    Timer time;
    for (int i = 0; i < v.size(); i++)// четные числа
    {


        if (v[i] % 2 == 0)
        {
            v.insert(v.begin() + i + 1, v[i]);
            i++;
        }
    }

    cout << "Time elapsed: " << time.elapsed() << '\n';

    cout<<endl;

    for (int i = 0; i < v.size(); i++)
        cout << v[i] << ' ';

    return 0;
}

// Лучший случай: тета n^2
// Средний случай: тета n^2
// Худший случай: тета n^2
// Реальное время работы: 2.35e-05
// Расчётное время работы: